package main

import (
	"testing"
	"strings"
	"net/http"
	"net/http/httptest"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"os"
)

func MockEntry()(p Entry) {
	p = Entry{
		Created: "foo",
		Location: "x,y",
		DeviceId: "deviceid",
		LocationAccuracy: "locationaccuracy",
		Ssid: "ssid",
		DeviceManufacturer: "devicemanufacturer",
		DeviceProduct: "deviceproduct",
		DeviceModel: "devicemodel",
	}
	return p
}

func TestToSlice(t *testing.T) {
	p := MockEntry()
	var s []string = p.ToSlice()
	if s[0] != `"foo"` {
		t.Fatalf("Location was '%s' not 'foo'", s[0])
	}
}

func TestToString(t *testing.T) {
	p := MockEntry()
	var s string = p.ToString()
	if ! strings.HasPrefix(s, `{"Created":`) {
		t.Fatalf("%s did not start with location json", s)
	}
}

func TestHealthHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/healthz", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(HealthHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("WiFi handler returned %v wanted %v", status, http.StatusOK)
	}

	expected := "OK"
	if rr.Body.String() != expected {
		t.Errorf("handler returned %v wanted %v", rr.Body.String(), expected)
	}
}

func TestWifiCheckinHandler(t *testing.T) {
	defer tearDown()
	p := MockEntry()
	body, _ := json.Marshal(p)
	req, err := http.NewRequest("POST", "/wifi", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(WifiCheckinHandler)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("WiFi handler returned %v wanted %v", status, http.StatusCreated)
	}

	r := Entry{}
	err = json.NewDecoder(rr.Body).Decode(&r)
	if err != nil {
		t.Error(err)
	}
	if r.Location != "x,y" {
		t.Errorf("Expected 'x,y' but got '%v' for Location", r.Location)
	}
	dat, err := ioutil.ReadFile(entriesFilePath)
	if err != nil {
		t.Fatal(err)
	}

	entry := []Entry{}
	json.Unmarshal(dat, &entry)
	if entry[0].Location != "x,y" {
		t.Errorf("Location '%s' did not match expected 'x,y'", entry[0].Location)
		t.Errorf("Full contents of file %v", string(dat))
	}
}

func tearDown() {
	os.Remove(entriesFilePath)
}

func TestListWifiHandler(t *testing.T) {
	defer tearDown()
	entry1 := MockEntry()
	entry2 := MockEntry()
	entry2.DeviceId = "device2"

	// Write first entry to file
	entries := []Entry{entry1}
	s, _ := json.Marshal(entries)
	ioutil.WriteFile(entriesFilePath, []byte(s), 0644)
	t.Logf("Writing entry1 to %s", entriesFilePath)


	// --- Ensure first entry is returned for list ---
	req, err := http.NewRequest("GET", "/wifi", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(WifiList)

	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("WifiHandler returned %v wanted %v", status, http.StatusOK)
	}

	var r []Entry
	err = json.NewDecoder(rr.Body).Decode(&r)
	if len(r) == 0 {
		t.Errorf("Returned json had 0 entries instead of 1")
	} else {
		if r[0].DeviceId != entry1.DeviceId {
			t.Errorf("Expected to get %s but got %s", entry1, r[0])
		}
	}

	t.Logf("Calling POST /wifi to add entry2")
	body, _ := json.Marshal(entry2)
	req2, err := http.NewRequest("POST", "/wifi", bytes.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}

	rr2 := httptest.NewRecorder()
	handler2 := http.HandlerFunc(WifiCheckinHandler)

	handler2.ServeHTTP(rr2, req2)


	// -- Call list endpoint and verify we get entry 1 and 2 back
	req3, err := http.NewRequest("GET", "/wifi", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr3 := httptest.NewRecorder()
	handler3 := http.HandlerFunc(WifiList)

	handler3.ServeHTTP(rr3, req3)

	if status := rr3.Code; status != http.StatusOK {
		t.Errorf("WifiHandler returned %v wanted %v", status, http.StatusOK)
	}

	var r2 []Entry
	err = json.NewDecoder(rr3.Body).Decode(&r2)
	if len(r2) != 2 {
		t.Errorf("Returned json had %d entries instead of 2", len(r2))
	} else {
		if r2[0].DeviceId != entry1.DeviceId {
			t.Errorf("Expected to get %s but got %s", entry1, r2[0])
		}
		if r2[1].DeviceId != entry2.DeviceId {
			t.Errorf("Expected to get %s but got %s", entry2, r2[0])
		}
	}
}
