package main

import(
	"net/http"
	"encoding/json"
	"log"
	"net/http/httputil"
	"os"
	"reflect"
	"strings"
	"time"
	"io/ioutil"
)

type Entry struct {
	Created string `json:-`
	Location string `json:"location"`
	DeviceId string `json:"device_id"`
	LocationAccuracy string `json:"location_accuracy"`
	Ssid string `json:"ssid"`
	DeviceManufacturer string `json:"device_manufacturer"`
	DeviceProduct string `json:"device_product"`
	DeviceModel string `json:"device_model"`
}

var entriesFilePath string = "thing.json"

func addNewEntry(filepath string, entry Entry) {
	// Load existing entries
	entries := LoadEntriesFile(filepath)
	entries = append(entries, entry)
	s, err := json.Marshal(entries)
	if err != nil {
		log.Fatal(err)
	}
	ioutil.WriteFile(filepath, []byte(s), 0644)
	log.Printf("Wrote %d entries to %s", len(entries), filepath)
}

func DumpRequest(w http.ResponseWriter, req *http.Request) {
	requestDump, err := httputil.DumpRequest(req, true)
	if err != nil {
		log.Println(err.Error())
	} else {
		log.Println(string(requestDump))
	}
}

func (p *Entry) ToString() (payload string) {
	userJson, err := json.Marshal(p)
	if err != nil {
		panic(err)
	}
    payload = string(userJson)
	return payload
}

func (p Entry) GetFields() (headers []string) {
    val := reflect.ValueOf(p)
    for i := 0; i < val.Type().NumField(); i++ {
        headers = append(headers, "\"" + val.Type().Field(i).Name + "\"")
    }
	log.Println(strings.Join(headers, ","))
	return headers
}

func (p Entry) ToSlice() (csv []string) {
    val := reflect.ValueOf(p)
    for i := 0; i < val.Type().NumField(); i++ {
        csv = append(csv, "\"" + val.Field(i).Interface().(string) + "\"")
    }
	return csv
}

func WifiCheckinHandler(w http.ResponseWriter, r *http.Request) {
	//DumpRequest(w, r)
	p := Entry{}

	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		panic(err)
	}

	p.Created = time.Now().String()

	log.Println(p.ToString())
	userJson, err := json.Marshal(p)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	w.Write(userJson)
	addNewEntry(entriesFilePath, p)
}

func WifiList(w http.ResponseWriter, r *http.Request) {
	entries := LoadEntriesFile(entriesFilePath)
	userJson, err := json.Marshal(entries)
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(userJson)
}

func WifiHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
		case http.MethodGet:
			WifiList(w, r)
		case http.MethodPost:
			WifiCheckinHandler(w, r)
	}
}

func HealthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
}


func LoadEntriesFile(filepath string)(entries []Entry) {
	if _, err := os.Stat(filepath); err != nil {
		log.Printf("Writing empty %s since it did not exist", filepath)
		ioutil.WriteFile(filepath, []byte("[]"), 0644)
	}
	jsonFile, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteValue, &entries)

	log.Printf("Loaded %d entries from %s", len(entries), filepath)
	return entries
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/wifi", WifiHandler)
	mux.HandleFunc("/healthz", HealthHandler)
	log.Println("Starting server on :5000")
	http.ListenAndServe(":5000", mux)
}
